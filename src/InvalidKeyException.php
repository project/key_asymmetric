<?php

namespace Drupal\key_asymmetric;

/**
 * An invalid key was submitted.
 */
class InvalidKeyException extends \RuntimeException {
}
